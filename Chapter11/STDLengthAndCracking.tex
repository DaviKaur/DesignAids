%############################ Chapter11  #############################%
\chapter{Shear, Torsion, Development Length and Cracking}
%%%%%%%%%%%%%%%%%%%%%%%%%%%% Section 11.1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} Provisions for Shear, Torsion, Development
Length and Cracking in the \citetitle{IS2000} are at variance with 
those given in the \citetitle{IS1964}. This has become necessary in the 
light of extensive research and consequential advancement in knowledge 
on these subjects. The provisions for shear in the \citetitle{IS2000} 
(clause 40) are similar to those given by CP—110\textsuperscript{15}.
Clause 41 of the \citetitle{IS2000} gives the limit state of collapse 
in torsion which is based on the foreign sources mentioned 
elsewhere\textsuperscript{33}. Ferguson\textsuperscript{18} has worked 
extensively on the problem of bond between concrete and steel bars 
which has given rise to the concept of development length of bars. 
Cracking has been dealt with in a general way by the \citetitle{IS2000}.
Shear is critical, generally, in beams. The critical section for design 
in shear with or without torsion is located at a distance of effective 
depth away from the face of supporting columns (clauses 22.6.2.1 and 
41.2 of the \citetitle{IS2000}). Solid slabs are generally safe in 
shear. Columns in buildings are also safe in shear. Footings however, 
need to be checked for shear. Torsion may occur in buildings in a 
variety of situations. Helicoidal stair beams, beams curved in plan, 
beams supporting cantilever canopies without connection with top floor 
slab etc., are some of the examples of members under torsion.
%%%%%%%%%%%%%%%%%%%%%%%%%%% Section 11.2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Limit State of Collapse in Shear}
%%%%%%%%%%%%%%%%%%%%%%%%%% Sub Section 11.2.1 %%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Procedure for Design}
Nominal shear stress for rectangular beams of uniform depth is given by,
%%%%%%%%%%%%%%%%%%%%%%%%%% Begin Equation 11.1%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{equation}
\label{eq:ShStress}		% Equation number 11.1
\tau_{v} = \frac{V_{u}}{bd}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%% End Equation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The value of nominal shear stress ($\tau_{v}$) is to be compared with 
allowable shear strength $\tau_{c}$ which depends on the concrete mix 
and the amount of tension steel provided at the section where shear is 
critical. \tablem 19 of the \citetitle{IS2000} gives values of to (the 
minimum value of $\tau_{c}$ is taken for $pt \leq 0.15$) which are 
valid for beams only. For slabs (except the ﬂat slabs), the allowable 
shear stress equals $k \tau_{c}(k \geq 1.0$), where the parameter $k$ 
is given by clause 40.2.1.1 of the \citetitle{IS2000}. \tablem 20 of 
the \citetitle{IS2000} gives the maximum shear strength to, max in 
beams, with or without shear reinforcement. For solid slabs, values of 
\tablem 20 are to be halved. The comparison of $\tau_{v}$ with 
$\tau_{c}$ and $\tau_{c,max}$, max gives rise to the following three 
cases of design.
%%%%%%%%%%%%%%%%%%%%%%%%%% Begin Listing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{enumerate}[(i)]
\item If $\tau_{v} < \tau_{c}$
lone is strong enough to resist shear. No shear reinforcement is 
required theoretically. But minimum shear reinforcement is to be 
provided in accordance with clause 26.5.1.6 of the \citetitle{IS2000}.
\item If $\tau_{v} > \tau_{c} < \tau_{c,max}$
shear reinforcement is required and is to be calculated in accordance 
with clause 40.4 of the \citetitle{IS2000}.
\item If $\tau_{v} > \tau_{c,max}$
the concrete section is to be re-dimensioned, i.e. either $b$ or $d$ or 
both are to be increased.
\end{enumerate}
%%%%%%%%%%%%%%%%%%%%%%%%% End Listing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Design of Shear Reinforcement}
Shear reinforcement provided in beams is of two types, in practice.
\begin{enumerate}[(a)]
\item Vertical stirrups in the form of closed hoops.
\item Bent-up bars spared from the longitudinal tension bars of beam.
\end{enumerate}
(Additional bent-up bars are costlier than stirrups)

Total shear at the critical section of a beam is to be jointly resisted 
by concrete, vertical stirrups and bent-up bars, if any. However, shear 
resisted by bent-up bars shall not be more than that resisted by the 
vertical stirrups at the section. Clause 40.4 of the \citetitle{IS2000} 
gives, the shear resistance provided by concrete as,
%%%%%%%%%%%%%%%%%%%%%%%%% Begin Equation 11.2 %%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{equation}
\label{eq:ShResistConc}		% Equation number 11.2
V_{c} = \tau_{c} . bd
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End Equation %%%%%%%%%%%%%%%%%%%%%%%%%%%%
the shear resistance provided by vertical stirrups as,
%%%%%%%%%%%%%%%%%%%%%%%%% Begin Equation 11.3 %%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{equation}
\label{eq:ShResistVS}		% Equation number 11.3
V_{us} = 0.87 fy . \frac{A_{sv}}{s_{v}} . d
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End Equation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
and the shear resistance provided by bent-up bars as,
%%%%%%%%%%%%%%%%%%%%%%%%% Begin Equation 11.4 %%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{equation}
\label{eq:ShResistB}		% Equation number 11.4	
Q = 0.87 fy . A_{sv} . \sin \alpha
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End Equation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In practice, $\alpha = \SI{45}{\degree}$ is generally adopted for 
bent-up bars. The shear equilibrium at the critical section gives,
\begin{equation}		% Equation number 11.5	
\label{eq:ShEquil}
V_{u} = V_{c} + V_{us} + Q
\end{equation}
When vertical stirrups only are provided (Q=0) \eqn (\ref{eq:ShEquil}) 
gives,
$$V_{us} = V_{u} - V_{c} = \tau_{v} . bd - \tau_{c} . bd$$
to,
\begin{equation}
\label{eq:SolShEquil}		% Equation number 11.6	
\frac{V_{us}}{d} = (\tau_{v} - \tau_{c})b
\end{equation}
\eqn (\ref{eq:ShResistVS}) and (\ref{eq:SolShEquil}) give the 
resistance of vertical stirrups as
\begin{equation}
\label{eq:ResistVS}		% Eqaution number 11.7
\frac{A_{sv}}{s_{v}} = \frac{V_{us}}{d} . \frac{1}{0.87 fy} = 
\frac{1}{0.87 fy} . (\tau_{v} - \tau_{c}) . b
\end{equation}
The units to be used in \eqn (\ref{eq:ResistVS}) should be $V_{us}$ in 
$kN, fy, \tau_{v}, \tau_{c}$ in $kN/cm^2$ and $b,d$ in $cm$.
\tablem (11.1) is used to choose suitable vertical stirrups. It gives 
values of $\frac{A_{sv}}{s_{v}}$ for various diameters of bars and 
spacing of stirrups.
\tablem (11.1) is independent of steel quality. The \citetitle{IS2000} 
prescribes that the area of stirrups shall not be less than the minimum 
shear reinforcement specified in clause 26.5.1.6 of the 
\citetitle{IS2000}, which gives,
\begin{subequations}
\begin{equation}
\label{eq:Result}		% Equation number 11.8
\frac{A_{sv}}{s_{v}} \geq \bigg(\frac{0.4}{0.87 fy}\bigg) . b
\end{equation}
In \eqn (\ref{eq:Result}) $fy$ is to be put in $N/mm^2$ and $b$ in 
$cm$. \tablem 11.1 can be used to get the minimum stirrups required. 
For wide beams $(b>d), b$ should be replaced by $d$ in \eqn 
(\ref{eq:Result}) to get the area of minimum shear reinforcement.
When both vertical stirrups and bent-up bars are provided at the 
critical section.
\begin{equation}
\label{eq:VSBcritical}		% Equation number 11.8a
Q \leq V_{us}
\end{equation}
\end{subequations}
\tablem 11.2 gives shear resistance of single bent-up bars of various 
diameters of steel types $Fe$ 250 $Fe$ 450 and it is based on \eqn 
(\ref{eq:ShResistB}). The value of \tablem 11.2 will be multiplied by 
the number of bent-up bars at the section to give $Q$. \eqn 
(\ref{eq:ShEquil}) gives,
$$V_{us} = V_{u} - V_{c} - Q$$
\begin{equation}
\label{eq:ResultShEquil}	% Equation number 11.9 derived from 11.5
\frac{V_{us}}{d} = \bigg[(\tau_{v} - \tau_{c} . b - \frac{Q}{d}\bigg]
\end{equation}
\eqn (\ref{eq:ShResistVS}) gives,
\begin{equation}
\label{eq:ResultResistVS}	% Equation number 11.10 derived from 
11.3
\frac{A_{sv}}{s_{v}} = \frac{V_{us}}{d} . \frac{1}{0.87 fy} = 
\frac{1}{0.87 fy} . \bigg[(\tau_{v} - \tau_{c}) . b - \frac{Q}{d}\bigg]
\end{equation}
\tablem 11.1 can be used for choosing the required vertical stirrups. 
Units should be properly put in \eqn (\ref{eq:ResultResistVS}). It 
should be kept in mind that bent-up bars are of no use when shear force 
is of an alternative nature as that due to earthquake or oscillating 
machine load. In these situations, only vertical stirrups will be 
effective as shear reinforcement. Also bent-up bars will not be fully 
effective when torsion is predmninent at a given section. Also, for 
torsion, four legged stirrups will not be as effective as two legged 
stirrups.

\section{Limit state of Collapse In Torsion}
In accordance with the \citetitle{IS2000}, a given torsional moment 
($T_{u}$) at the critical section is replaced by, an equivalent shear 
($V_{t}$) and an equivalent moment ($M_{t}$), where,
\begin{equation}
\label{eq:Vt}			% Equation number 11.11
V_{t} = 1.6\frac{T_{u}}{b}
\end{equation}
\begin{equation}
\label{eq:Mt}			% Equation number 11.12
M_{t} = \frac{T_{u}}{1.7}\bigg(1 + \frac{D}{b}\bigg)
\end{equation}
\subsection{Design for shear combined with Torsion and Moment}
The equivalent shear for shear ($V_{u}$) combined with torsional moment 
($T_{u}$) is given by (clause 41.3.1 of the \citetitle{IS2000}),
\begin{equation}
\label{eq:Ve}			% Equation number 11.13
V_{e} = V_{u} = V_{t}
\end{equation}
The equivalent bending moment for moment ($M_{u}$) combined with 
torsion ($T_{u}$) is given by (clause 41.4.2 of the \citetitle{IS2000}),
\begin{equation}
\label{eq:Me}			% Equation number 11.14
M_{e} = M_{u} \pm M_{t}
\end{equation}
When $M_{t} > M_{u}$ in \eqn (\ref{eq:Me}), one value of $M_{e}$ will 
be negative and the steel bars will have to be provided also on the 
compression face of section.
\subsection{Reinforcement for Members under Torsion}
For members under torsion combined with moment and shear, longitudinal 
bars and
closed vertical stirrups are to be provided. The critical section is to 
be designed for an equivalent shear $V_{e}$ and an equivalent moment 
$M_{e}$ given by \eqns (\ref{eq:Ve}) and (\ref{eq:Me}) respectively.
The section is to be designed for $V_{e}$ exactly in the same way as 
explained in Article 11.2, with the only proviso that bent-up bars are 
not to be used, when torsional effect is predominant.
Further, clause 41.4.3 gives,
\begin{equation}
\label{r1}			% Equation number 11.15
\frac{A_{sv}}{s_{v}} = frac{1}{0.87 fy}\bigg[\frac{T_{u}}{b_{1}d_{1}} + 
\frac{V_{u}}{2.5d_{1}}\bigg]
\end{equation}
\begin{equation}
\label{r2}			% Equation number 11.16
\frac{A_{sv}}{s_{v}} = \frac{1}{0.87 fy} . (\tau_{ve} - \tau_{c}) . b
\end{equation}
where,
\begin{equation}
\label{tauVe}			% Equation number 11.17
\tau_{ve} = \frac{V_{e}}{bd}
\end{equation}
The greater value of $\frac{A_{sv}}{s_{v}}$ given by \eqns (\ref{eq:r1}) and (\ref{eq:r2}) is to be taken for design and \tablem 
11.1 is used to choose the diameter and spacing of vertical stirrup 
required. The spacing of vertical stirrups shall be checked with clause 
26.5.1.7 of the \citetitle{IS2000}.
Longitudinal bars in the section are calculated by designing the 
section for a bending moment $M_{e}$ given by \eqn (\ref{eq:Me}) with 
the help of Charts 2.1 and 2.2 of Chapter 2. Additional side face 
reinforcement equal to 0.1\% of the web area of section is to be 
provided when $b$ or $D$ exceeds 45 $cm$ as per clause 26.5.1.7 of the 
\citetitle{IS2000}. Examples are given in Article 11.6 to illustrate 
the procedure for design of reinforcement.
\section{Development Length of Bars}
Bond stress concept of the \citetitle{IS1964} is replaced by the 
concept of development length in the \citetitle{IS2000}. For a bar to 
develop full strength in tension or compression at a given section, the 
bar must continue on either side by a length at least equal to 
development length ($L_{d}$) which is given by clause 26.2.1 of the 
\citetitle{IS2000} as,
\begin{equation}
\label{eq:Ld}			% Equation number 11.18
L_{d} = \frac{\Phi.\sigma_{s}}{4 \tau_{bd}}
\end{equation}
where $\sigma_{s}, \tau_{bd}$ are stressed at the ultimate stage. With 
$\sigma_{s} = 0.87 fy$ (full strength) and $\tau_{bd}$ by clause 
26.2.1.1 of the \citetitle{IS2000}, \eqn (\ref{Ld}) is written as,
\begin{equation}
\label{eq:Ldmax}		% Equation number 11.19
L_{d,max} = n . \Phi
\end{equation}
where,
\begin{equation}
\label{eq:n}
n = \bigg(\frac{0.87 fy}{4 \tau_{bd}}\bigg)
\end{equation}
\tablem (\ref{eq:ShResistVS}) gives values of $n$ for various types of 
reinforcing steel, concrete mixes and nature of stress (compression or 
tension) in bars, for full strength $\sigma_{s} = 0.87 fy$. When 
$\sigma_{s} < 0.87 fy$, values of $n$ will be less than those given by 
\tablem (\ref{eq:ShResistVS}).
\subsection{Uses of development length}
Development length of bars is to be checked in the following situations 
in the detailing of reinforced concrete structures.
\begin{enumerate}[(i)]
\item Anchoring of bars with anchorage length equal to $L_{d}$ for the 
appropriate value of $\sigma_{s}$ (clause 26.2.2).
\item Length of bars beyond the point of curtailment of tension bars in 
bending members (clause 26.2.3).
\item Lap length of bars for splicing reinforcement (clause 26.2.5).
\item Checking length of footing bars for adequate development length 
beyond the critical section (clause 34.2.4.3).
\item Checking length of dowel bars of columns for development length 
beyond the critical section (clause 33.4.2). The projected total length 
of a bar in compression including books, bends, straight length etc., 
shall be considered for development length (clause 26.2.2.2). It is in 
line with ACI \citetitle{IS2000}\textsuperscript{27}.
\end{enumerate}

\section{Limit State of Cracking}
Cracking occurs in bending members and beam-type-columns. Cracking not 
only mars the appearance of members but may also affect their 
durability by permitting eventual corrosion of steel bars under 
atmospheric action. Cracking of concrete in tension zone can not be 
avoided, it can only be limited by adhering to the requirements of 
minimum steel area (clause 26.5) and spacing of steel bars (clause 
26.3) in members. Further proper and prolonged curing of concrete 
(clause 13.5) and good quality concrete mix (clause 8.0) should be 
ensured for controlling cracking. All these measures required for 
controlling cracking in reinforced concrete members are part of the 
general construction and detailing practice and no special effort is 
needed on this account in design of structures excepting for water 
retaining structures which are dealt with separately in Chapter 14. 
\tablem 11.4 gives the minimum steel area in members as per clause 26.5 
of the \citetitle{IS2000}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Examples %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Examples}
\begin{example}
Design of Section for Shear\textsuperscript{34}\\
\textbf{Data.}
\begin{sagesilent}
var('b,d,V,f_ck,f_y,A_s')
b = 20
d = 40
V = 70
f_ck = 15
f_y = 250
A_s = 18.47
\end{sagesilent}

$$b = \sage{b} cm$$     $$f_{ck} = \sage{f_ck} N/mm^2$$
$$d = \sage{d} cm$$     $$f_{y} = \sage{f_y} N/mm^2$$
$$V = \sage{V} kN$$     $$A_{s} = \sage{A_s} cm^2$$

\textbf{Required:} Design of Vertical stirrups\\
\textbf{Solution:} \eqn (\ref{eq:ShStress}) gives,



\end{example}

\section{Conclusion}
Provisions for shear, torsion, development length and cracking in the 
\citetitle{IS2000} are quite different from those given in the 
\citetitle{IS1964}. Procedure for design of rectangular reinforced 
concrete sections under shear with or without torsion and bending 
moment is given along with examples to illustrate it. It is seen that 
the \citetitle{IS2000} gives much less shear reinforcement in beams 
than that by the \citetitle{IS1964}.

