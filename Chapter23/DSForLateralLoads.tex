%############################# Chapter 23 #############################%
\chapter{Design of Structures For Lateral Loads}
\section{Introduction}Structures are built to support loads. Dead and live loads give vertical loads while wind, earthquake, temperature changes and blasts cause horizontal or lateral loads. Design of structures for vertical loads is comparatively simple, while the same for lateral loads is difficult and time-consuming. A building needs to be considered as a whole under lateral loads\textsuperscript{1}, while for vertical loads, frames or walls in the building can be considered as seperate units. The main purpose of a building is to support vertical loads. For this reason, structures should be planned properly for resisting vertical loads. The lateral loads are occasional loads, which may act on a structure once in a while. For this reason, permissible stresses in structural materials are increased or partial safety load factors are reduced when the structure is designed for the combined effect of vertical and horizontal loads. The cost of structures also goes up when the effect of lateral loads is included in the structural design. The aim of this chapter is to consider in detail the design of structures for lateral loads.
\section{Design for Vertical Loads}
Structures are designed for both the vertical and the horizontal (or lateral) loads. Vertical or gravity loads consisting of the dead and the live loads are the reason for the existence of structures. The dead loads act on a structure all the time while the live loads varying from 0 to 100\% of their values also act on the structure all the time. The dead load of a structure is about 80\% of the value of the total vertical load, the rest 20\% being atrributed to the live load. Since the vertical loads act on the structure all the time, the design of structure for vertical loads should be done carefully, accurately and economically.
Building design for vertical loads is quite simple and straight-forward. The loads from slabs go to beams and columns (or load bearing walls in brick or concrete) by the principle of tributary area. Vertical elements like columns and/or walls spread the load on the supporting soil with the help of footings provided at their bases, keeping in view that the soil bearing capacity is not exceeded, which also indirectly, controls the soil settlement.
A reinforced concrete framed building is divided into a number of frames with loads calculated from slab and wall loadings on the basis of the tributary area and the frames are analysed by 2-D computer programs. When the slab system is one-way spanning, main frames are required in one principal direction only, the frames in the other principal direction being formed by tie-beams only. This is required for horizontal loads, it is not the requirement of vertical loads. However, when the slab system is two-way spanning, frames in both the principal directions are formed and these are necessary for both the vertical and the horizontal loads.
\section{Design for Horizontal Loads}
The horizontal loads due to wind and earthquake are occasional loads. Wind, in general, acts on a structure all the time but the maximum wind load in a storm occurs once in a while. Such conditions may occur 5 to 10 times in the life-time of a structure, which may be taken to be one hundred years. The earthquake too may occur 4 to 6 times in the life-span of a structure. Since these loads are occasional loads, permissible stresses in structural materials like concrete and steel are increased by one-third in the working stress method of design and the partial safety load factors are reduced by one-fourth in the limit state method of design\textsuperscript{2}. This approach stands good in reason and keeps the cost of structures within reasonable limits. Wind and earthquake are both erratic in their nature. These loads may act in any direction on a building. So, it becomes neccessary to hold the vertical resisting elements together rigidly. This is done by the reinforced concrete floors (including roof) by their diaphragm action in plan, as the plan dimensions of building are, in general, quite large and the floor or roof beams act as stiffeners to the slab diaphragms. In order to cater for the erratic nature of the horizontal loads, it is also necessary to design a building in the two principal directions separately for the given amounts of horizontal loads. These aspects are specially required for design of building for lateral loads.

% ######## Figure ######## %

A building acts as a unit under horizontal loads. The floor diaphragms distribute the acting horizontal loads (or shear) to the resisting frames in proportion to their stiffnesses or rigidities. this is at variance with the principle of tributary area used in the  design for vertical loads. At each floor level, the frames or walls deflect by the same amount due to the diaphragm action of the floor under concentric horizontal shear (Fig. 23.1). When the applied horizontal shear is eccentric with respect to the centre of rigidity (Fig. 23.2), the floor diaphragm not only translates but it also rotates in plan, so that the external applied shear gives additional $\pm$ shears in frames. For design of frames, only additive shear effects are considered and substractive shear effects are neglected. This is the effect of torsion in buildings due to eccentric horizontal loads\textsuperscript{3}. 3-D computer programs can be directly used to take care of this effect in structural members. But it is easier said than done. In practice, buildings are planned to be symmetrical in order to eliminate torsion in plan and to achieve economy in structural design. The distribution of the applied horizontal load due to wind or earthquake to the various frames or shear walls in proportion to their rigidities, taking torsion in plan into account may be called allocation analysis, which is not even mentioned in many text-books, neither it is taught in engineering colleges, where only frame analysis under given horizontal loads is given due importance.

% ###### Figure ###### %

\section{Partial Safety Load Factors for Limit State Design}
\tablem 18 of IS:456-2000 (called the \citetitle{IS2000}\textsuperscript{2}) gives the following load combinations for the limit states of collapse:
%%%%%%%%%%%%% Begin Eauations %%%%%%%%%%%%%%
\begin{equation}
\label{eq:U1}			% Equation number 23.1
U = 1.5 (DL + LL)
\end{equation}
\begin{equation}
\label{eq:U2}			% Equation number 23.2
U = (1.5 or 0.9) DL + 1.5 WL
\end{equation}
\begin{equation}
\label{eq:U3}			% Equation number 23.3
U = 1.2 (DL + LL +WL)
\end{equation}
%%%%%%%%%%%%% End Equations %%%%%%%%%%%%%%%%%
with a note that while considering earthquake effects, $WL$ may be substituted by EL in the above equations. The factor $1.2$ in \eqn (\ref{eq:U3}) is given by 0.75 x 1.5 = 1.125 = 1.2 (about 7\% more than the required value).
SP:24-1983, \textit{Explanatory Hand Book}\textsuperscript{4} on the \citetitle{IS2000} states in its explanation of Clause 36.4.1 of the \citetitle{IS2000} that \eqn (\ref{eq:U2}) may apply to chimneys and cooling towers where the lateral loading (wind or earthquake) is the primary imposed load. Without going into the controversy of primary or secondary imposed loadings, it should be clearly noted that \eqn (\ref{eq:U2}) given above is not applicable to buildings for which \eqn (\ref{eq:U3}) is applicable. As mentioned earlier, live load $(LL)$ may have a value varying from 0 to 100\% of its full value. 
%%%%%%%%%% Begin Equations %%%%%%%%%%
\eqn (\ref{eq:U2}) gives,
\begin{equation}
\label{eq:U2result}		% Equation number 23.4
U = 1.5 (DL +WL)
\end{equation}
\eqn (\ref{eq:U3}) gives for $LL$ = 0
\begin{equation}
\label{eq:U3result}		% Equation number 23.5
U = 1.2 (DL + WL)
\end{equation}
%%%%%%%%%% End Equation %%%%%%%%%%
Both the above \eqns (\ref{eq:U2result}) and (\ref{eq:U3result}) are based on \tablem 11 of the \citetitle{IS2000} and these lead to inconsistent results. For bringing some order to this aspect, the explanation of SP:24 should be considered valid and building should be designed only for load combinations given by \eqns (\ref{eq:U1}) and (\ref{eq:U3}), the \eqn (\ref{eq:U2}) being inapplicable to buildings. If \eqn (\ref{eq:U2}) is insisted upon in building design, as may be done by some unthinking engineers, it will govern the design leading to a substantial increase in the cost of structures. This happens because live load is quite small compared to dead load, it may be of the order of 20\% of the total load.
Temperature and shrinkage effects may be neglected in ordinary short length buildings\textsuperscript{2}. For long buildings, expansion joints should be provided to produce a few shortlength buildings.
In certain areas where open-cast mining methods are used, blasting is done continually all day and night. The effect of a given blast is a fraction (or a multiple) of the effect of earthquake in the area\textsuperscript{5}. For structural design of members, a load factor of 1.5 should be considered for the effect of blast loading (BL) and it is assumed that the worst effects of earthquake, wind and blast do not occur at the same time. Buildings in blast-affected areas should be designed on the basis of \eqn (\ref{eq:U3}), together with the following additional \eqn
%%%%%%%%%% Begin Equation %%%%%%%%%%
\begin{equation}
\label{eq:Ufinal}		% Equation number 23.6
U = 1.5 (DL + LL + BL)
\end{equation}
%%%%%%%%%% End Equation %%%%%%%%%%
and the greater area of steel got by using \eqns (\ref{eq:U3}) and (\ref{eq:Ufinal}) shall be provided in structural members. Blast-resistant building will require some more steel in members than that required for vertical only.
\section{Structural Systems for Lateral Loads in Reinforced Concrete Buildings}
For low-rise buildings, structural systems adopted for vertical loads \textemdash load bearing brick walls or reinforced concrete frames \textemdash are to be checked for the effect of lateral loads taking a partial safety load factor of 1.2 in the limit state design of members. When the buildings are tall, lateral loads govern the design and frames together with shear walls and shear cores will provide efficient structural systems.
The structural systems for both the vertical and the horizontal loads used in practice, are given below:
%%%%%%%%%%% Begin Listing  %%%%%%%%%%
\begin{enumerate}[(i)]
\item Load bearing brick walls: Useful for houses, hostels, schools etc. valid for about 4 storeys and less
\item Mixed system of brick walls reinforced concrete columns: Useful for large houses and bungalows etc.
\item Framed structure: Useful for apartments and office buildings for 8 storeys and less
\item Shear walled structure: Useful for tall buildings for 20 storeys and less
\item Mixed framed and shear walled structure: Useful for taller buildings
\item Shear cored and bundled cored structure: Useful for very tall buildings
\end{enumerate}
%%%%%%%%%%% End listing %%%%%%%%%%%%%
Useful details of structural systems for tall buildings have been recently given by Dr.N.Subramanian\textsuperscript{6}.
By including the effect of lateral loads in the structural design, beams and columns are directly affected. More steel bars than required for the vertical load design will be required to be provided in beams and columns. Slab panels remain unaffected. Plinth beams may be designed for column-end moments due to lateral loads, in which case, footings will also be unaffected by lateral loads. If plinth beams are not provided, footings shall be checked for extra column moments at base due to lateral loads and the allowable soil pressure shall then be increased by 25\%. In general, cost of reinforced concrete buildings gets increased by including the effect of lateral loads in the structural design.
\section{Structural Systems for Steel Industrial Buildings}
With steel trusses or N-girders resting on reinforced concrete or steel columns, adequate bracing shall be provided at the bottom chord level (Fig. 23.3) to simulate the effect of floor diaphragm in reinforced concrete buildings. Bracing in the upper chord level in the end bays together with purlins also help maintain the integrity and unity of the building against lateral loads. The columns under lateral loads act as cantilevers fixed at their footings (Fig. 23.4). For wind on long side of building, trussed frames will be designed under tributery lateral wind pressure with both columns acting as vertical cantilevers (Fig. 23.4). The column and the footings design will be affected by the wind moments. For wind on short side, bracing at the eaves level will form a gable-end girder in plan which will span between the longitudinal frames formed on the long sides of buildings (Fig. 23.3). Industrial buildings are generally single storeyed with a sheeted roof of a light loading. Steel design is based on working stress method of design where one-third increase in permissible stresses will be made for including the wind effects in the structural design of members?
The difference in the structural behaviour of single-storey steel and reinforced concrete buildings in shown in Fig. 23.5. Columns in the steel buildings are vertical cantilevers fixed at base, while in the reinforced concrete buildings, portal action takes place. Design of columns and footings becomes different in the two systems under discussion. Vertical bracing in a bay in steel building simulates the action of a shear wall in reinforced concrete buildings (Fig. 23.6).
\section{Conclusion}
An overview of the design of reinforced concrete and steel buildings for lateral loads has been given in this paper. The crux of the problem of design of buildings is the effect of lateral loads. A rational view has been taken of the partaial safety load factors to be used in the design of buildings. This subject has been considered in detail elsewhere\textsuperscript{8}.
\section{References}
\begin{enumerate}
\item The Applications of Moment Distribution. The Concrete Association of India, 1957 (Page 104), Bombay.
\item The Indian Standard Code of Practice for Plain and Reinforced Concrete (Fourth Revision), IS: 456-2000. Bureau of Indian Standards, New Delhi.
\item The Indian Standard Criteria for Earthquake Resistant Design of Structures (Fourth Revision), IS: 1893-1984. Bureau of Indian Standards, New Delhi.
\item Explanatory Hand Book on Indian Standard Code of Practice for Plain and Reinforced Concrete, IS: 456-1978, SP: 24-1983. Bureau of Indian Standards, New Delhi.
\item Indian Standard Criteria for Safety and Design of Structures subject to Underground Blasts, IS: 6922-1973, Bureau of Indian Standards, New Delhi.
\item Subramanian N.Computer Analysis and Design of Tall Structures. CE and CR, April, 1995 pp. 42-46.
\item Indian Standard Code of Practice for Use of Structural Steel in General Building Construction, IS:800-1984. Bureau of Indian Standards. New Delhi.
\item Varyani, U.H. "Structural Design of Multistoreyed Buildings", 1999. South Asian Publishers Private Limited, New Delhi.
\end{enumerate}
